public class Fish extends Animal{

    private String isSwimming;

    public String getIsSwimming() {
        return isSwimming;
    }

    public void setIsSwimming(String isSwimming) {
        this.isSwimming = isSwimming;
    }
}
