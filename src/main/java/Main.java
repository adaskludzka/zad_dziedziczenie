public class Main {
    public static void main(String[] args) {

        Bird bird = new Bird();
        bird.setName("Jacek");
        bird.setWeight(2);
        bird.setSpecies("Jaskółka");
        bird.setIsSinging("Tak");



        Fish fish = new Fish();
        fish.setName("Adam");
        fish.setWeight(1);
        fish.setSpecies("Pielęgnica");
        fish.setIsSwimming("Tak");
    }
}
