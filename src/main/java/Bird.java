public class Bird extends Animal{

    private String isSinging;

    public String getIsSinging() {
        return isSinging;
    }

    public void setIsSinging(String isSinging) {
        this.isSinging = isSinging;
    }
}
